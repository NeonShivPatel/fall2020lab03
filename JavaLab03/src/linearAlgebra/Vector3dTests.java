//@author Shiv Patel
//@ID: 1935098

package linearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {
	@Test
	void  testGets() {
		Vector3d vector = new Vector3d(1,2,3);
		System.out.println("Vector's X is equal to " + vector.getX());
		System.out.println("Vector's Y is equal to " + vector.getY());
		System.out.println("Vector's Z is equal to " + vector.getZ());
	}

	@Test
	void  testMagnitude() {
		Vector3d vector = new Vector3d(1,2,3);
		System.out.println("The magnitude of the vector is " + vector.magnitude());
	}
	
	@Test
	void  testDotProduct() {
		Vector3d vector = new Vector3d(1,2,3);
		Vector3d vec = new Vector3d(1,2,3);
		System.out.println("The dotProduct result is " + vector.dotProduct(vec));
	}
	
	@Test
	void  testAdd() {
		Vector3d vector = new Vector3d(1,2,3);
		Vector3d vec = new Vector3d(1,2,3);
		Vector3d vector2 = new Vector3d(vector.add(vec).getX(),vector.add(vec).getY(),vector.add(vec).getZ());
		System.out.println("Vector2's X is equal to " + vector2.getX());
		System.out.println("Vector2's Y is equal to " + vector2.getY());
		System.out.println("Vector2's Z is equal to " + vector2.getZ());
	}
}
