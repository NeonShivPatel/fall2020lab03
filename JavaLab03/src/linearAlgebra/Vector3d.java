//@author Shiv Patel
//@ID: 1935098
package linearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	public Vector3d (double a, double b, double c) {
		this.x=a;
		this.y=b;
		this.z=c;
	}
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public double magnitude() {
		return Math.sqrt(Math.pow(this.getX(),2)+Math.pow(this.getY(),2)+Math.pow(this.getZ(),2));
	}
	
	public double dotProduct(Vector3d vec) {
		return this.x*vec.getX()+this.y*vec.getY()+this.z*vec.getZ();
	}
	
	public Vector3d add(Vector3d vec) {
		double a = this.x + vec.getX();
		double b = this.y + vec.getY();
		double c = this.z + vec.getZ();
		Vector3d vector2 = new Vector3d(a, b, c);
		return vector2;
	}
	
	public static void main (String[] args) {
		Vector3d vector = new Vector3d(1,2,3);
		Vector3d vec = new Vector3d(1,2,3);
		System.out.println("Vector's X is equal to " + vector.getX());
		System.out.println("Vector's Y is equal to " + vector.getY());
		System.out.println("Vector's Z is equal to " + vector.getZ());
		System.out.println("The magnitude of the vector is " + vector.magnitude());
		System.out.println("The dotProduct result is " + vector.dotProduct(vec));
		Vector3d vector2 = new Vector3d(vector.add(vec).getX(),vector.add(vec).getY(),vector.add(vec).getZ());
		System.out.println("Vector2's X is equal to " + vector2.getX());
		System.out.println("Vector2's Y is equal to " + vector2.getY());
		System.out.println("Vector2's Z is equal to " + vector2.getZ());
	}
}
